<?php

namespace Deployer;

require 'recipe/laravel.php';

// Project repository
set('repository', 'git@bitbucket.org:amoffett/sugar-rush.git');

// Http User
set('http_user', 'www-data');

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', ['public/posters']);
set('allow_anonymous_stats', false);

// Hosts
host('production')
    ->hostname('sugarrush.papertrail.uk')
    ->set('deploy_path', '/var/www/sugarrush.papertrail.uk')
    ->set('branch', 'master')
    ->user('root');

    
// Tasks
// task('build', function () {
//     run('cd {{release_path}} && build');
// });

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
before('deploy:symlink', 'artisan:migrate');

task('artisan:optimize', function () {});

