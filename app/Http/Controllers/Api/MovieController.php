<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Movie;

class MovieController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \App\Models\Movie::paginate(8);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title'       => 'required',
            'category'    => 'required',
            'description' => 'required',
            'poster'      => 'required',
        ]);

        $posterPath = '';

        if ($request->hasFile('poster')) {
            $posterPath = $request->poster->store('posters', 'public');
        }

        $movieData = [
            'title'       => $request->input('title'),
            'category'    => $request->input('category'),
            'description' => $request->input('description'),
            'poster'      => $posterPath,
        ];

        $movie = Movie::create($movieData);

        return response()->json([
            'success' => true,
            'message' => 'Movie successfully added',
            'movie'   => $movie,
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        return $movie;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {

        $request->validate([
            'title'       => 'required',
            'category'    => 'required',
            'description' => 'required',
            'poster'      => 'required',
        ]);

        if ($request->hasFile('poster')) {
            $posterPath    = $request->poster->store('posters', 'public');
            $movie->poster = $posterPath;
        }

        $movie->title       = $request->input('title');
        $movie->category    = $request->input('category');
        $movie->description = $request->input('description');
    
        $success = $movie->update();

        if (!$success) {
            return response()->json([
                'success' => false,
                'message' => 'Movie update failed'
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Movie updated',
            'movie'   => $movie
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        return $movie->delete();
    }

}
