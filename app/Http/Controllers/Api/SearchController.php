<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Movie;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $movies = \App\Models\Movie::where('title', 'like', '%' . $request->get('query') . '%');
        
        if ($request->get('category') != 0) {
            $movies->where('category', $request->get('category'));
        }
                                    
        return $movies->paginate(8);
    }

}
