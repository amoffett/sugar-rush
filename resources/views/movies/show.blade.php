@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        	<h1>{{ $movie->title }}</h1>
        	<img src="{{ $movie->poster_url }}" alt="Poster" width="300">
        	<p>{{ $movie->description }}</p>
        </div>
    </div>
</div>
@endsection
