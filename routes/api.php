<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware(['api'])->group(function () {

	Route::post('/search', [App\Http\Controllers\Api\SearchController::class, 'search'])->name('search');

});


Route::middleware(['auth:api'])->group(function () {

	Route::resource('movies', App\Http\Controllers\Api\MovieController::class);

});
